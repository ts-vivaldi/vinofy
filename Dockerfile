FROM node:18-alpine

# Create app directory
WORKDIR /usr/src/app

RUN apk add bash
RUN apk add yarn

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./

RUN yarn install

EXPOSE 80

CMD [ "yarn", "run", "start" ]